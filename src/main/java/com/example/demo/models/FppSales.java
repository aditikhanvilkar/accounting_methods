package com.example.demo.models;

public class FppSales {
	
	private String fppTickets;
	
	private String fppRetailPercent;
	
	private String fppTicketPercent;
	
	private String coverageOption;

	public String getFppTickets() {
		return fppTickets;
	}

	public void setFppTickets(String fppTickets) {
		this.fppTickets = fppTickets;
	}

	public String getFppRetailPercent() {
		return fppRetailPercent;
	}

	public void setFppRetailPercent(String fppRetailPercent) {
		this.fppRetailPercent = fppRetailPercent;
	}

	public String getFppTicketPercent() {
		return fppTicketPercent;
	}

	public void setFppTicketPercent(String fppTicketPercent) {
		this.fppTicketPercent = fppTicketPercent;
	}

	public String getCoverageOption() {
		return coverageOption;
	}

	public void setCoverageOption(String coverageOption) {
		this.coverageOption = coverageOption;
	}
	
	
	
	

}
