package com.example.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ConfigurationData") 
public class FurnitureConfigData {
	
	@Id 
	@GeneratedValue
	@Column(name = "id")
	private int id;
	
	private String configName;
	
	private String  configValue;

	@Column(name = "configName", nullable = true)
	public String getConfigName() {
		return configName;
	}

	public void setConfigName(String configName) {
		this.configName = configName;
	}
	@Column(name = "configValue", nullable = true)
	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	

}
