package com.example.demo.models;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Column; 
import javax.persistence.Entity; 


@Entity
@Table(name = "Roles") 
public class Roles {

	int roleId;
	String roleName;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Column(name = "RoleName", nullable = false)
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	
}
