package com.example.demo.models;

public class FppRebateCost {
	
	private String planCost;
	private String rebateCost;
	private String netCost;
	private String rebatePerCost;
	public String getPlanCost() {
		return planCost;
	}
	public void setPlanCost(String planCost) {
		this.planCost = planCost;
	}
	public String getRebateCost() {
		return rebateCost;
	}
	public void setRebateCost(String rebateCost) {
		this.rebateCost = rebateCost;
	}
	public String getNetCost() {
		return netCost;
	}
	public void setNetCost(String netCost) {
		this.netCost = netCost;
	}
	public String getRebatePerCost() {
		return rebatePerCost;
	}
	public void setRebatePerCost(String rebatePerCost) {
		this.rebatePerCost = rebatePerCost;
	}
	
	

}
