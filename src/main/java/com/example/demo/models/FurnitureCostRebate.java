package com.example.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ConfigurationData") 
public class FurnitureCostRebate {
	
	
	private int id;
	private String coverageCondition;
	private String remit;
	private String rebate;
	private String loss;
	private String reserve;
	private String fees;
	private String rap;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "CoverageCondition", nullable = true)
	public String getCoverageCondition() {
		return coverageCondition;
	}
	public void setCoverageCondition(String coverageCondition) {
		this.coverageCondition = coverageCondition;
	}
	
	@Column(name = "Remit", nullable = true)
	public String getRemit() {
		return remit;
	}
	public void setRemit(String remit) {
		this.remit = remit;
	}
	
	@Column(name = "Rebate", nullable = true)
	public String getRebate() {
		return rebate;
	}
	public void setRebate(String rebate) {
		this.rebate = rebate;
	}
	
	@Column(name = "Loss", nullable = true)
	public String getLoss() {
		return loss;
	}
	public void setLoss(String loss) {
		this.loss = loss;
	}
	
	@Column(name = "Reserve", nullable = true)
	public String getReserve() {
		return reserve;
	}
	public void setReserve(String reserve) {
		this.reserve = reserve;
	}
	
	@Column(name = "Fees", nullable = true)
	public String getFees() {
		return fees;
	}
	public void setFees(String fees) {
		this.fees = fees;
	}
	
	@Column(name = "RAP", nullable = true)
	public String getRap() {
		return rap;
	}
	public void setRap(String rap) {
		this.rap = rap;
	}
	
	
	

}
