package com.example.demo.models;

import javax.persistence.GeneratedValue;
import javax.persistence.Column; 
import javax.persistence.Entity; 

import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.Table;

 @Entity
 @Table(name = "Users") 
public class Users {

	int userID;
	int roleID;
	String userName;
	String recoveryEmail;
	
	String userPassword;
	String loginUserId;
	
	public String showLoggedUserId() {
		return loginUserId;
	}
	
	public void addLoggedUserId(String loginUserId1) {
		loginUserId = loginUserId1;
	}
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	@Column(name = "UserName", nullable = false)
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name = "RecoveryEmail", nullable = false)
	public String getRecoveryEmail() {
		return recoveryEmail;
	}
	public void setRecoveryEmail(String recoveryEmail) {
		this.recoveryEmail = recoveryEmail;
	}
	

	
	@Column(name = "UserPassword", nullable = false)
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	@Column(name = "RoleID", nullable = false)
	public int getRoleID() {
		return roleID;
	}
	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}
	
	public boolean equals(Object o){
		//System.out.println("In equals " +"value is :" +this.empName);
		Users user = (Users)o;
		if(user.getUserID() ==  this.userID){
		return true;
		}
		return false;
		}

	public String showUserIdString() {
		return String.valueOf(userID);
	}
	
}
