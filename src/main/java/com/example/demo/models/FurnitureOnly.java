package com.example.demo.models;




public class FurnitureOnly {
	
	private String furnitureTicketCnt;
	
	private String  furnitureTicketValue;
	
	private String furnitureTicketGrossMargin;

	
	public String getFurnitureTicketCnt() {
		return furnitureTicketCnt;
	}

	public void setFurnitureTicketCnt(String furnitureTicketCnt) {
		this.furnitureTicketCnt = furnitureTicketCnt;
	}
	
	
	public String getFurnitureTicketValue() {
		return furnitureTicketValue;
	}

	public void setFurnitureTicketValue(String furnitureTicketValue) {
		this.furnitureTicketValue = furnitureTicketValue;
	}


	public String getFurnitureTicketGrossMargin() {
		return furnitureTicketGrossMargin;
	}

	public void setFurnitureTicketGrossMargin(String furnitureTicketGrossMargin) {
		this.furnitureTicketGrossMargin = furnitureTicketGrossMargin;
	}
	
	

}
