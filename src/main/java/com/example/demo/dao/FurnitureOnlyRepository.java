package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.models.FurnitureConfigData;
import com.example.demo.models.FurnitureOnly;



@Repository
public interface FurnitureOnlyRepository extends JpaRepository<FurnitureConfigData, Integer> {
	
	 List<FurnitureConfigData> findAll();
	 
	 FurnitureConfigData findByConfigName(String configName);
	

}
