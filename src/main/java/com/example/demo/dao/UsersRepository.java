package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.example.demo.models.Users;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer>{

	 List<Users> findByrecoveryEmail(String email);
	 
}
