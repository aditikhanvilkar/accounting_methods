package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.example.demo.models.FurnitureCostRebate;

@Repository
public interface FurnitureCostRebateRepository extends JpaRepository<FurnitureCostRebate, Integer> {
	
	 List<FurnitureCostRebate> findAll();
	 List<FurnitureCostRebate> findByCoverageCondition(String coverageCondition);

}

