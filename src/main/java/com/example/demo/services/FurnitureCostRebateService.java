package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.FurnitureCostRebateRepository;
import com.example.demo.models.FurnitureCostRebate;

@Service
public class FurnitureCostRebateService {
	
	@Autowired
	FurnitureCostRebateRepository fppCostRebateRepo;
	
	public List<FurnitureCostRebate> findAllFppRebateData() {
		return fppCostRebateRepo.findAll();
	}
	
	public List<FurnitureCostRebate> findByCoverageCondition(String coverageCondition){
		return fppCostRebateRepo.findByCoverageCondition(coverageCondition);
	}

}
