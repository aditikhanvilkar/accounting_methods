package com.example.demo.services;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Service;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.UsersRepository;
import com.example.demo.models.Users;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class UsersServices {
	@Autowired
	UsersRepository userRepo;
	 @Autowired
	    private BCryptPasswordEncoder bCryptPasswordEncoder;

	  @Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW,isolation=Isolation.READ_UNCOMMITTED,rollbackFor=SQLException.class)
	  public Users addUserToDatabase(Users user) {
		  
		    user.setUserPassword(bCryptPasswordEncoder.encode(user.getUserPassword()));
		
		  return  userRepo.save(user);
	  }
	  
	  public List<Users>findByEmail(String email){
		  return userRepo.findByrecoveryEmail(email);
	  }
	  
	 
	  public Users findByUserId(int userId) {
		  List<Users>userList = userRepo.findAll();
		  for(Users user : userList) {
			  if(user.getUserID()==userId) {
				  return user;
			  }
		  }
		 return null;
	  }
	  
	  public Users updateUserToDatabase(Users user) {
		  return  userRepo.save(user);
		  }
	  
	  
	  public void deleteByUserId(int userId) {
		  userRepo.deleteById(userId);}
}
