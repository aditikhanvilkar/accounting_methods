package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.RolesRepository;
import com.example.demo.models.Roles;



@Service
public class RolesServices {

	@Autowired
	RolesRepository rolesRepo;
	
	public Roles addRolesToDatabase(Roles roles) {
		return rolesRepo.save(roles);
	}
	
	public Roles getByRoleId(int roleId) {
		return rolesRepo.findById(roleId).get();
	}
	
	public List<Roles>findByRoleName(String roleName){
		return rolesRepo.findByroleName(roleName);
	}
}
