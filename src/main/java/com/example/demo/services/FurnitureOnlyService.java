package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.FurnitureOnlyRepository;
import com.example.demo.models.FurnitureConfigData;
import com.example.demo.models.FurnitureOnly;





@Service
public class FurnitureOnlyService {
	
	@Autowired
	FurnitureOnlyRepository furnitureOnlyRepo;
	
	public List<FurnitureConfigData> findAllFurnitureData() {
		return furnitureOnlyRepo.findAll();
	}

	
	public FurnitureConfigData findConfigDataByName(String configName) {
		return furnitureOnlyRepo.findByConfigName(configName);
	}
	
	public FurnitureConfigData addFurnitureConfigData(FurnitureConfigData fppData) {
		return furnitureOnlyRepo.save(fppData);
	}

}
