package com.example.demo.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;



@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	  @Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;
	  
	  @Autowired private DataSource dataSource;
	  
	  @Bean public BCryptPasswordEncoder bCryptPasswordEncoder() { return new
	  BCryptPasswordEncoder(); }
	 
	

	
	/*
	 * public AuthenticationSuccessHandler authenticationSuccessHandler;
	 * 
	 * @Autowired public WebSecurityConfig(AuthenticationSuccessHandler
	 * authenticationSuccessHandler) { this.authenticationSuccessHandler =
	 * authenticationSuccessHandler; }
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/**").permitAll()
				.antMatchers("/error").permitAll()
				.antMatchers("/loginPage", "/ShowTicketData", "/ShowTableData", "/SetConfigData", "/ShowAdminPage","/ShowAdminFppPage").permitAll()
			//.antMatchers("/ShowAdminPage").access("hasRole('ROLE_ADMIN')")
		//		.antMatchers("/ShowTicketData").access("hasRole('ROLE_USER')")
				.antMatchers("/lib/bootstrap/**", "/css/**", "/img/**", "/js/**", "/slick/**","/admin/**").permitAll().anyRequest()
				 .authenticated()
	                .and()
	                .formLogin()
	           //     .loginPage("/loginPage").defaultSuccessUrl("/ShowTicketData").failureUrl("/loginPage?error")
	                .usernameParameter("username").passwordParameter("password")	
	                .loginPage("/loginPage")
	                .loginProcessingUrl("/loginPage")
	            	.defaultSuccessUrl("/ShowTicketData",true)
	            	.failureUrl("/loginPage?error=true").permitAll().and().logout().invalidateHttpSession(true)
					.deleteCookies("JSESSIONID").permitAll();
	    		//	.and()
	             //   .successHandler(authenticationSuccessHandler)
	              //  .permitAll()
	               // .and()
	               // .logout().logoutSuccessUrl("/loginPage?logout")
	              //  .and().csrf().disable();
	}
	
	 @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		/*
		 * auth .inMemoryAuthentication()
		 * .withUser("user").password("{noop}user@1234").authorities("ROLE_USER") .and()
		 * .withUser("admin").password("{noop}admin@1234").authorities("ROLE_USER",
		 * "ROLE_ADMIN");
		 */
	    }

	/*
	 * @Bean
	 * 
	 * @Override public UserDetailsService userDetailsService() { UserDetails user =
	 * User.withDefaultPasswordEncoder().username("user").password("password").roles
	 * ("USER") .build();
	 * 
	 * return new InMemoryUserDetailsManager(user); }
	 * 
	 * @Override public void configure(WebSecurity web) {
	 * 
	 * web.ignoring().antMatchers("/resources/**", "/static/**"); }
	 */

	 @Override
		public void configure(WebSecurity web) {
			
			web.ignoring().antMatchers("/resources/**", "/static/**");
		}
	 
	@Override protected void configure(AuthenticationManagerBuilder auth) throws
	  Exception {
	  
	  String USERS_QUERY =
	  "select [UserName], [UserPassword],1 from Users where [UserName]=? ";
	 
	  String USER_QUERY_EMAIL = " select [RecoveryEmail], [UserPassword],1 from Users where [RecoveryEmail]=? ";
	
	  
	  auth.jdbcAuthentication().usersByUsernameQuery(USERS_QUERY).
	  authoritiesByUsernameQuery(USER_QUERY_EMAIL).dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
	  //.passwordEncoder(bCryptPasswordEncoder);
	  

	 
	  
	  }

	
	  @Bean public AuthenticationManager customAuthenticationManager() throws
	  Exception { 
		  return authenticationManager(); 
		  }
	 
}
