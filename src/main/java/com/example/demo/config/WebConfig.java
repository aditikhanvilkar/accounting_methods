package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


  @Configuration
  public class WebConfig implements WebMvcConfigurer {
  
  
  
  
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/photos/**", "/css/**", "/img/**", "/js/**", "/assets/**", "/slick/**")
				.addResourceLocations("classpath:/static/css/", "classpath:/static/img/", "classpath:/static/js/",
						"classpath:/static/assets/", "classpath:/static/admin/js",
						"classpath:/static/admin/assets", "classpath:/static/admin/img", "classpath:/static/admin/css");
	}

	public void addViewControllers(ViewControllerRegistry registry) {

		
		/*
		 * registry.addViewController("/").setViewName("login");
		 * registry.addViewController("/ShowTicketData").setViewName("index");
		 * registry.addViewController("/adminIndex").setViewName("adminIndex");
		 * registry.addViewController("/loginPage").setViewName("login");
		 */
	}
  
  }
 