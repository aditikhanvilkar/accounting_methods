package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.models.FppRebateCost;
import com.example.demo.models.FurnitureConfigData;
import com.example.demo.models.FurnitureCostRebate;
import com.example.demo.models.FurnitureOnly;
import com.example.demo.services.FurnitureCostRebateService;
import com.example.demo.services.FurnitureOnlyService;
import com.fasterxml.jackson.databind.ObjectMapper;




@RestController
public class MainController {
	
	@Autowired
	FurnitureOnlyService furnitureOnlyService;
	
	@Autowired
	FurnitureCostRebateService fppCostRebateService;

	@GetMapping("/ShowTicketData")
	public ModelAndView ShowTicketData(Model model) {
		 ModelAndView modelAndView = new ModelAndView("index.html"); 
		 
		 List<FurnitureConfigData> furnitureConfigData = new ArrayList<FurnitureConfigData>();
		 furnitureConfigData =  furnitureOnlyService.findAllFurnitureData();
		 
		 FurnitureOnly fOnly =  new FurnitureOnly();
		 for(FurnitureConfigData fo:furnitureConfigData) {
			 if(fo.getConfigName().equalsIgnoreCase("FurnitureTickets"))
				 fOnly.setFurnitureTicketCnt(fo.getConfigValue());
			 else if(fo.getConfigName().equalsIgnoreCase("FurnitureTicketValue")) {
				 fOnly.setFurnitureTicketValue(fo.getConfigValue());
			 }
			 else if(fo.getConfigName().equalsIgnoreCase("FurnitureGMValue")) {
				 fOnly.setFurnitureTicketGrossMargin(fo.getConfigValue());
			 }
			 
		 }
		 
		 modelAndView.addObject("fOnly", fOnly);
		 

		 
		 List<FurnitureCostRebate> listFppRebate = new ArrayList<FurnitureCostRebate>();
		 listFppRebate =  fppCostRebateService.findByCoverageCondition("good");
		 
		
		/*
		 * for(FurnitureCostRebate fppCostRebate:listFppRebate) {
		 * 
		 * }
		 */
		 		 
		 FppRebateCost data1 =  new FppRebateCost();
		 data1.setPlanCost("35");
		 data1.setRebateCost("20");
		 data1.setNetCost("15");
		 data1.setRebatePerCost("57");
		 
		 FppRebateCost data2 =  new FppRebateCost();
		 data2.setPlanCost("36");
		 data2.setRebateCost("21");
		 data2.setNetCost("16");
		 data2.setRebatePerCost("58");
		 
		 
		 FppRebateCost data3 =  new FppRebateCost();
		 data3.setPlanCost("34");
		 data3.setRebateCost("19");
		 data3.setNetCost("14");
		 data3.setRebatePerCost("56");
		 
		 Map<String, List<FppRebateCost>> fppRebateDataByCoverageOption = new HashMap<>();
		 List<FppRebateCost> listOfFppData = new ArrayList<FppRebateCost>() ;
		 
		 List<FppRebateCost> listOfFppData1 = new ArrayList<FppRebateCost>() ;
		
		 listOfFppData.add(data1);
		 listOfFppData.add(data2);
		 
		 listOfFppData1.add(data1);
		 listOfFppData1.add(data3);
		 
		 fppRebateDataByCoverageOption.put("Good", listOfFppData);
		 fppRebateDataByCoverageOption.put("Better", listOfFppData1);
		 String jsonRebate = "";
		 
		 ObjectMapper mapper = new ObjectMapper();
		 try {

			 jsonRebate = mapper.writeValueAsString(fppRebateDataByCoverageOption);
			 System.out.println("Json" + jsonRebate);

			} catch (Exception ex) {
				System.out.println("Json" + jsonRebate);
			}
		 
		 modelAndView.addObject("fpprebatemap", jsonRebate);
		
		return modelAndView;
	//	return "Hello World";
	}
	
	
	@RequestMapping(value = "getFppCoverageOption", method = RequestMethod.GET)
	public @ResponseBody List<String> getFppCoverageOption() {
		//
		List<String> coverageOptionList = new ArrayList<String>() ;
			
			
		coverageOptionList.add("Combo Furniture- Good, Stains & Some Damage");
		coverageOptionList.add("Combo Furniture -  Better, Most Damages");
		coverageOptionList.add("Combo Furniture  - Best , Most Damage & Failure");
		coverageOptionList.add("Combo Furniture -  Max , No Inclusions");

		

		return coverageOptionList;
	}
	
	
	@GetMapping("/ShowTableData")
	public ModelAndView ShowTableData(Model model) {
		 ModelAndView modelAndView = new ModelAndView("p&l-dashboard.html"); 
		 
		
		return modelAndView;
	//	return "Hello World";
	}
	
	

}
