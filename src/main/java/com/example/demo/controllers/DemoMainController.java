package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.models.FppRebateCost;
import com.example.demo.models.FurnitureConfigData;
import com.example.demo.models.FurnitureCostRebate;
import com.example.demo.models.FurnitureOnly;
import com.example.demo.models.Roles;
import com.example.demo.models.Users;
import com.example.demo.services.FurnitureCostRebateService;
import com.example.demo.services.FurnitureOnlyService;
import com.example.demo.services.RolesServices;
import com.example.demo.services.UsersServices;
import com.fasterxml.jackson.databind.ObjectMapper;







@RestController
public class DemoMainController {
	
	@Autowired
	FurnitureOnlyService furnitureOnlyService;
	
	@Autowired
	FurnitureCostRebateService fppCostRebateService;
	@Autowired
	UsersServices userServices;
	
	@Autowired
	RolesServices roleServices;
	
	@RequestMapping(value = { "/"}, method = RequestMethod.GET)
	public ModelAndView loginPage() {
		ModelAndView model = new ModelAndView("login.html");
		
		

		/*
		 * Users user = new Users(); user.setRecoveryEmail("testuser@gns.com");
		 * user.setUserName("testuser"); user.setRoleID(1);
		 * user.setUserPassword("testuser@123"); user =
		 * userServices.addUserToDatabase(user);
		 * 
		 * Users user1 = new Users(); user1.setRecoveryEmail("testadmin@gns.com");
		 * user1.setUserName("testadmin"); user1.setRoleID(2);
		 * user1.setUserPassword("testadmin@123"); user1 =
		 * userServices.addUserToDatabase(user1);
		 */
		return model;
	}
	
	/*
	 * @RequestMapping(value = "/loginPage", method = RequestMethod.GET) public
	 * String loginPage(Model model, String error, String logout) {
	 * 
	 * // ModelAndView model = new ModelAndView(); if (error != null) {
	 * model.addAttribute("error", "Invalid Credentials provided."); }
	 * 
	 * if (logout != null) { model.addAttribute("message",
	 * "Logged out  successfully."); }
	 * 
	 * return "login.html"; }
	 */

	@RequestMapping(value = "/loginPage", method = RequestMethod.GET)
	public ModelAndView loginPage(@RequestParam(value = "error",required = false) String error,
	@RequestParam(value = "logout",	required = false) String logout, HttpServletRequest request) {
		
		 HttpSession session = request.getSession(false);
	        String errorMessage = null;
	        if (session != null) {
	            AuthenticationException ex = (AuthenticationException) session
	                    .getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	            if (ex != null) {
	                errorMessage = ex.getMessage();
	            }
	        }
		
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid Credentials provided.");
		}

		if (logout != null) {
			model.addObject("message", "Logged out  successfully.");
		}

		model.setViewName("login.html");
		return model;
	}
	
	/*
	 * @RequestMapping("/error") public ModelAndView errorView(){ return new
	 * ModelAndView("login.html"); }
	 */

	@GetMapping("/ShowTicketData")
	public ModelAndView ShowTicketData(Model model) {
		 ModelAndView modelAndView = new ModelAndView("index.html"); 
		/*
		 * UserDetails userDetails = (UserDetails)
		 * SecurityContextHolder.getContext().getAuthentication().getPrincipal(); Users
		 * loggedUser = new Users(); List<Users> userList = new ArrayList<Users>();
		 * 
		 * 
		 * userList = userServices.findByEmail(userDetails.getUsername()); loggedUser =
		 * userList.get(0); Roles role =
		 * roleServices.getByRoleId(loggedUser.getRoleID()); if
		 * (role.getRoleName().equalsIgnoreCase("user")) {
		 * 
		 * modelAndView.setViewName("index.html"); String strUserId =
		 * String.valueOf(loggedUser.getUserID());
		 * 
		 * // modelAndView.addObject("loggedDealer",dealerServices.findByUserId(String.
		 * valueOf(loggedUser.getUserID()))); //modelAndView.addObject("loggedDealer",
		 * dealerServices.findByUserId(strUserId)); } else {
		 * modelAndView.setViewName("adminIndex.html"); modelAndView.addObject("fOnly",
		 * new FurnitureOnly()); }
		 */
		 
		 List<FurnitureConfigData> furnitureConfigData = new ArrayList<FurnitureConfigData>();
		 furnitureConfigData =  furnitureOnlyService.findAllFurnitureData();
		 
		 FurnitureOnly fOnly =  new FurnitureOnly();
		 for(FurnitureConfigData fo:furnitureConfigData) {
			 if(fo.getConfigName().equalsIgnoreCase("FurnitureTickets"))
				 fOnly.setFurnitureTicketCnt(fo.getConfigValue());
			 else if(fo.getConfigName().equalsIgnoreCase("FurnitureTicketValue")) {
				 fOnly.setFurnitureTicketValue(fo.getConfigValue());
			 }
			 else if(fo.getConfigName().equalsIgnoreCase("FurnitureGMValue")) {
				 fOnly.setFurnitureTicketGrossMargin(fo.getConfigValue());
			 }
			 
		 }
		 
		 modelAndView.addObject("fOnly", fOnly);
		 

		 
		 List<FurnitureCostRebate> listFppRebate = new ArrayList<FurnitureCostRebate>();
		 listFppRebate =  fppCostRebateService.findByCoverageCondition("good");
		 
		
		/*
		 * for(FurnitureCostRebate fppCostRebate:listFppRebate) {
		 * 
		 * }
		 */
		 		 
		 FppRebateCost data1 =  new FppRebateCost();
		 data1.setPlanCost("35");
		 data1.setRebateCost("20");
		 data1.setNetCost("15");
		 data1.setRebatePerCost("57");
		 
		 FppRebateCost data2 =  new FppRebateCost();
		 data2.setPlanCost("36");
		 data2.setRebateCost("21");
		 data2.setNetCost("16");
		 data2.setRebatePerCost("58");
		 
		 
		 FppRebateCost data3 =  new FppRebateCost();
		 data3.setPlanCost("34");
		 data3.setRebateCost("19");
		 data3.setNetCost("14");
		 data3.setRebatePerCost("56");
		 
		 Map<String, List<FppRebateCost>> fppRebateDataByCoverageOption = new HashMap<>();
		 List<FppRebateCost> listOfFppData = new ArrayList<FppRebateCost>() ;
		 
		 List<FppRebateCost> listOfFppData1 = new ArrayList<FppRebateCost>() ;
		
		 listOfFppData.add(data1);
		 listOfFppData.add(data2);
		 
		 listOfFppData1.add(data1);
		 listOfFppData1.add(data3);
		 
		 fppRebateDataByCoverageOption.put("Good", listOfFppData);
		 fppRebateDataByCoverageOption.put("Better", listOfFppData1);
		 String jsonRebate = "";
		 
		 ObjectMapper mapper = new ObjectMapper();
		 try {

			 jsonRebate = mapper.writeValueAsString(fppRebateDataByCoverageOption);
			 System.out.println("Json" + jsonRebate);

			} catch (Exception ex) {
				System.out.println("Json" + jsonRebate);
			}
		 
		 modelAndView.addObject("fpprebatemap", jsonRebate);
		
		return modelAndView;
	//	return "Hello World";
	}
	
	
	@RequestMapping(value = "getFppCoverageOption", method = RequestMethod.GET)
	public @ResponseBody List<String> getFppCoverageOption() {
		//
		List<String> coverageOptionList = new ArrayList<String>() ;
			
			
		coverageOptionList.add("Combo Furniture- Good, Stains & Some Damage");
		coverageOptionList.add("Combo Furniture -  Better, Most Damages");
		coverageOptionList.add("Combo Furniture  - Best , Most Damage & Failure");
		coverageOptionList.add("Combo Furniture -  Max , No Inclusions");

		

		return coverageOptionList;
	}
	
	
	@GetMapping("/ShowTableData")
	public ModelAndView ShowTableData(Model model) {
		 ModelAndView modelAndView = new ModelAndView("p&l-dashboard.html"); 
		 
		
		return modelAndView;
	//	return "Hello World";
	}
	
	@RequestMapping("/SetConfigData")
	@Transactional(rollbackFor = Exception.class, noRollbackFor = org.springframework.transaction.UnexpectedRollbackException.class)
	public ModelAndView SetConfigData(@ModelAttribute FurnitureOnly fOnly,
			@RequestParam("furnitureTicketCnt") String furTktCnt, 
			@RequestParam("furnitureTicketValue") String furTktVal,
			@RequestParam("furnitureTicketGrossMargin") String furTktGm,  HttpSession session,
			HttpServletRequest request, BindingResult errors) {
		 ModelAndView modelAndView = new ModelAndView("adminIndex.html"); 
		 
		
		 FurnitureConfigData furnitureConfigData = new FurnitureConfigData();
		 
		 try {
			 
			 fOnly.setFurnitureTicketCnt(furTktCnt);
			 fOnly.setFurnitureTicketValue(furTktVal);
			 fOnly.setFurnitureTicketGrossMargin(furTktGm);
			 
		 if(furTktCnt!=null) {
			 furnitureConfigData = furnitureOnlyService.findConfigDataByName("FurnitureTickets");
			 
			
			 furnitureConfigData.setConfigValue(furTktCnt);
			 furnitureOnlyService.addFurnitureConfigData(furnitureConfigData);
		 
		 }
		 if(furTktVal!=null) {
			 furnitureConfigData = furnitureOnlyService.findConfigDataByName("FurnitureTicketValue");
		//	 furnitureConfigData.setConfigName("FurnitureTicketValue");
			 furnitureConfigData.setConfigValue(furTktVal);
			 furnitureOnlyService.addFurnitureConfigData(furnitureConfigData);
		 }
		 if(furTktGm!=null) {
			 furnitureConfigData = furnitureOnlyService.findConfigDataByName("FurnitureGMValue"); 
			// furnitureConfigData.setConfigName("FurnitureGMValue");
			 furnitureConfigData.setConfigValue(furTktGm);
			 furnitureOnlyService.addFurnitureConfigData(furnitureConfigData);
		 }
		 modelAndView.addObject("successMsg", "Data added successfully");
		 modelAndView.addObject("fOnly",fOnly);
		 }catch(Exception e) {
				modelAndView.addObject("failureMsg", "Data Not added successfully");
		 }
		 
		return modelAndView;
	//	return "Hello World";
	}
	
	@GetMapping("/ShowAdminPage")
	public ModelAndView ShowAdminPage(Model model) {
		 ModelAndView modelAndView = new ModelAndView("adminIndex.html"); 
		 
			modelAndView.addObject("fOnly", new FurnitureOnly());
		return modelAndView;
	//	return "Hello World";
	}
	
	@GetMapping("/ShowAdminFppPage")
	public ModelAndView ShowAdminFppPage(Model model) {
		 ModelAndView modelAndView = new ModelAndView("admin-fpp-sales.html"); 
		 
		
		return modelAndView;
	//	return "Hello World";
	}


}
